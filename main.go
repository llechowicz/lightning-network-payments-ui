package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

// Context ...
type Context map[string]interface{}

func isDebug(r *http.Request) bool {
	return strings.Contains(r.Host, "localhost")
}

func index(w http.ResponseWriter, r *http.Request) {
	cxt := Context{"data": r.FormValue("data")}
	switch r.Method {
	case http.MethodGet:
		tpl, _ := template.ParseFiles("templates/index.html")
		tpl.Execute(w, cxt)
	case http.MethodPost:
		newInvoice(w, r)
	}
}

func generatePaymentRequest(description string, satoshi int, expiry int) string {
	cmd := fmt.Sprintf(
		"lncli addinvoice --memo=\"%s\" --amt=%d --expiry=%d",
		description,
		satoshi,
		expiry,
	)
	fmt.Println(description, satoshi, expiry)
	output, _ := exec.Command("bash", "-c", cmd).Output()
	var data map[string]interface{}
	json.Unmarshal(output, &data)

	return data["pay_req"].(string)
}

func newInvoice(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	amount, _ := strconv.Atoi(r.FormValue("amount"))
	description := r.FormValue("description")
	expiry, _ := strconv.Atoi(r.FormValue("expiry"))

	var address string
	if isDebug(r) {
		address = strconv.Itoa(int(time.Now().UnixNano()))
	} else {
		address = generatePaymentRequest(
			description,
			amount,
			expiry,
		)
	}

	redirectURL := fmt.Sprintf("/?data=%s", address)
	http.Redirect(w, r, redirectURL, http.StatusFound)
}

func generateQRCodeImage(w http.ResponseWriter, r *http.Request) {
	data := r.FormValue("data")
	cmd := fmt.Sprintf("qrcode %s", data)
	output, err := exec.Command("bash", "-c", cmd).Output()

	if err != nil {
		fmt.Fprintf(w, err.Error())
		return
	}

	w.Header().Set("Content-Type", "image/jpeg")
	w.Write(output)
}

func main() {
	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	http.HandleFunc("/qr/", generateQRCodeImage)
	http.HandleFunc("/", index)

	log.Println(http.ListenAndServe(":9090", nil))
}
